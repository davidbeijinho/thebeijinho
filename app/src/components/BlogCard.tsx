import Link from "next/link";
import Tag from "@/components/Tag";

export default function BlogCard({
  blog,
}: {
  blog: {
    category: string;
    slug: string;
    title: string;
    excerpt: string;
    date: string;
    tags: string[];
  };
}) {
  return (
    <div className="py-8 md:flex md:flex-wrap md:flex-nowrap border-b border-gray-300">
      <div className="mr-4 mb-6 flex-shrink-0 flex flex-col">
        <span className="font-semibold title-font text-gray-700">
          {blog.category}
        </span>
        <span className="mt-1 text-gray-500 text-sm">
          {blog.date.substring(0, 10)}
        </span>
      </div>
      <div className="block md:flex-grow">
        <h2 className="text-2xl font-medium text-gray-900 title-font mb-2">
          {blog.title}
        </h2>
        <div className="my-4">
          {blog.tags.map((tag) => (
            <Tag tag={tag} key={tag} />
          ))}
        </div>
        <p className="leading-relaxed">{blog.excerpt}</p>
        <Link
          href={`/blog/${blog.slug}`}
          className="text-orange-600 inline-flex items-center mt-4"
        >
          Learn More
        </Link>
      </div>
    </div>
  );
}
