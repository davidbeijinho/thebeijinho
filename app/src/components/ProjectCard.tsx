import Image from "next/image";
import Link from "next/link";

export default function ProjectCard({
  project,
}: {
  project: {
    slug: string;
    title: string;
    imageSrc: string;
    imageAlt: string;
  };
}) {
  return (
    <div className="group relative">
      <div className="aspect-h-1 aspect-w-1 w-full overflow-hidden rounded-md bg-gray-200 lg:aspect-none group-hover:opacity-75 lg:h-80">
        <Link href={`/projects/${project.slug}`}>
          <Image
            width={500}
            height={1000}
            src={project.imageSrc}
            alt={project.imageAlt}
            className="h-full w-full object-cover object-center lg:h-full lg:w-full"
          />
        </Link>
      </div>
      <div className="mt-4 flex justify-between">
        <div>
          <h3 className="text-md text-gray-700 mb-1">
            <Link href={`/projects/${project.slug}`}>{project.title}</Link>
          </h3>
        </div>
      </div>
    </div>
  );
}
