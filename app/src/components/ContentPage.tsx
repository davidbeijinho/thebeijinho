import Image from "next/image";
import Tag from "@/components/Tag";

export default function ContentPage({
  children,
  title,
  imageSrc,
  tags,
}: {
  children: React.ReactNode;
  title: string;
  imageSrc: string;
  tags: string[];
}) {
  return (
    <div className="lg:grid lg:grid-cols-3 lg:grid-rows-[auto,auto,1fr] py-24 sm:py-32">
      <div className="lg:col-span-2">
        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl mb-2">
          {title}
        </h2>
        <div className="my-3 flex flex-wrap">
          {tags.map((tag) => (
            <Tag tag={tag} key={tag} />
          ))}
        </div>
      </div>
      <div className="lg:row-span-3 lg:ml-4">
        <Image
          src={imageSrc}
          width={500}
          height={1000}
          alt={title}
          className="h-full w-full object-cover object-center"
        />
      </div>
      <div className="lg:col-span-2 mt-6">{children}</div>
    </div>
  );
}
