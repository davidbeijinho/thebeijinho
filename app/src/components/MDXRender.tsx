"use client";
import Tag from "@/components/Tag";
import { MDXRemote, type MDXRemoteSerializeResult } from "next-mdx-remote";
const components = {
  Tag,
};

export default function MDXRender({
  content,
}: {
  content: MDXRemoteSerializeResult;
}) {
  return <MDXRemote {...content} components={components} />;
}
