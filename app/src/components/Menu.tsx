import MenuLink from "@/components/MenuLink";

export default function Menu() {
  return (
    <>
      <MenuLink href="/">About</MenuLink>
      <MenuLink href="/blog">Blog</MenuLink>
      <MenuLink href="/projects">Projects</MenuLink>
      <MenuLink href="/contacts">Contacts</MenuLink>
    </>
  );
}
