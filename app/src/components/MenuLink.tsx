"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";

export default function MenuLink({
  children,
  href,
}: {
  children: React.ReactNode;
  href: string;
}) {
  const pathName = usePathname().split("/")?.[1];
  return (
    <Link
      href={href}
      className={`
        ${"/" + pathName == href ? "bg-gray-900 text-white" : "text-gray-300 hover:bg-gray-700 hover:text-white"}
        block rounded-md px-3 py-2 text-base font-medium text-white`}
      aria-current="page"
    >
      {children}
    </Link>
  );
}
