"use client";
import { MenuIcon, XIcon } from "lucide-react";
import { useState } from "react";
import Menu from "./Menu";
import Link from "next/link";
import Image from "next/image";
export default function NavBar() {
  const [isOpen, setOpen] = useState(false);
  return (
    <nav className="bg-gray-800">
      <div className="mx-auto max-w-7xl px-2 sm:px-6 lg:px-8">
        <div className="relative flex h-16 items-center justify-between">
          <div className="absolute inset-y-0 left-0 flex items-center sm:hidden">
            <button
              onClick={() => setOpen(!isOpen)}
              type="button"
              className="relative inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white"
              aria-controls="mobile-menu"
              aria-expanded="false"
            >
              <span className="absolute -inset-0.5"></span>
              <span className="sr-only">Open main menu</span>
              <MenuIcon
                className={`
                ${isOpen ? "hidden" : "block"}
                block h-6 w-6`}
                aria-hidden="true"
              />
              <XIcon
                className={`
          ${isOpen ? "block" : "hidden"}
          h-6 w-6`}
                aria-hidden="true"
              />
            </button>
          </div>
          <div className="flex flex-1 items-center justify-center sm:items-stretch sm:justify-start">
            <div className="flex sm:flex-shrink-0 sm:grow items-center">
              <Link href="/">
                <Image
                  className="h-8 w-auto inline align-middle"
                  src="/images/logo.svg"
                  alt="Your Company"
                  width={100}
                  height={100}
                />
                <h2 className="align-middle inline text-2xl font-bold leading-7 ml-1 text-white sm:truncate sm:text-3xl">
                  TheBeijinho
                </h2>
              </Link>
            </div>
            <div className="hidden sm:ml-6 sm:block">
              <div className="flex space-x-4">
                <Menu />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className={`
  ${isOpen ? "block" : "hidden"}
  `}
        id="mobile-menu"
      >
        <div className="space-y-1 px-2 pb-3 pt-2">
          <Menu />
        </div>
      </div>
    </nav>
  );
}
