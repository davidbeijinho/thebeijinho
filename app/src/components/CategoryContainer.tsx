export default function CategoryContainer({
  children,
  title,
  subTitle,
}: {
  children: React.ReactNode;
  title: string;
  subTitle: string;
}) {
  return (
    <div className="mx-auto max-w-7xl py-24 sm:py-32">
      <div className="mx-auto max-w-2xl lg:mx-0">
        <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl mb-2">
          {title}
        </h2>
        <p className="text-lg leading-8 text-gray-600">{subTitle}</p>
      </div>
      {children}
    </div>
  );
}
