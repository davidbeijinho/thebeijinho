import Link from "next/link";

export default function Tag({ tag, href }: { tag: string; href?: string }) {
  if (href) {
    return (
      <Link
        href={href}
        className="rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100 mr-2 my-1"
      >
        {tag}
      </Link>
    );
  }
  return (
    <span className="rounded-full bg-gray-50 px-3 py-1.5 font-medium text-gray-600 hover:bg-gray-100 mr-2 my-1">
      {tag}
    </span>
  );
}
