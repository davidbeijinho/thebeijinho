import type { Metadata } from "next";
import { Inter as FontSans } from "next/font/google";
import "./globals.css";
import { cn } from "@/lib/utils";
import Layout from "@/components/Layout";

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
});

export const metadata: Metadata = {
  title: "TheBeijinho.com",
  description: "My small place on the web",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className="h-full">
      <head>
        <script
          defer
          data-domain="thebeijinho.com"
          src="https://plausible.panda-nomada.thebeijinho.com/js/script.outbound-links.tagged-events.local.js"
        ></script>
      </head>
      <body
        className={cn(
          "h-full min-h-screen bg-background font-sans antialiased",
          fontSans.variable,
        )}
      >
        <Layout>{children}</Layout>
      </body>
    </html>
  );
}
