import Image from "next/image";
import { GithubIcon, GitlabIcon, MailIcon, LinkedinIcon } from "lucide-react";

export default function Page() {
  return (
    <div className="mx-auto max-w-7xl py-24 sm:py-32">
      <div className="mx-auto grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 sm:gap-y-20 lg:mx-0 lg:max-w-none lg:grid-cols-2">
        <div className="lg:pr-8">
          <div className="lg:max-w-lg">
            <h2 className="text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl mb-2">
              Where to find me
            </h2>
            <p className="text-lg leading-8 text-gray-600">
              On the web, i mean.
            </p>
            <p className="mt-6 text-lg leading-8 text-gray-600">
              Here is a list of places where you can see what i share on the
              web.
            </p>
            <dl className="mt-10 max-w-xl space-y-8 text-base leading-7 text-gray-600 lg:max-w-none">
              <div className="relative pl-9">
                <dt className="inline font-semibold text-gray-900">
                  <GitlabIcon
                    className="absolute left-1 top-1 h-5 w-5 text-orange-600"
                    aria-hidden="true"
                  />
                  GitLab.
                </dt>
                <dd className="inline">
                  A social git hub that is open source:{" "}
                  <a
                    className="text-orange-600"
                    href="https://gitlab.com/davidbeijinho"
                    target="_blank"
                    rel="noreferrer"
                  >
                    https://gitlab.com/davidbeijinho
                  </a>
                </dd>
              </div>
              <div className="relative pl-9">
                <dt className="inline font-semibold text-gray-900">
                  <GithubIcon
                    className="absolute left-1 top-1 h-5 w-5 text-orange-600"
                    aria-hidden="true"
                  />
                  Github.
                </dt>
                <dd className="inline">
                  The open-source social network that is not open source, but is
                  popular:{" "}
                  <a
                    className="text-orange-600"
                    href="https://github.com/davidbeijinho"
                    target="_blank"
                    rel="noreferrer"
                  >
                    https://github.com/davidbeijinho
                  </a>
                </dd>
              </div>
              <div className="relative pl-9">
                <dt className="inline font-semibold text-gray-900">
                  <LinkedinIcon
                    className="absolute left-1 top-1 h-5 w-5 text-orange-600"
                    aria-hidden="true"
                  />
                  LinkedIn.
                </dt>
                <dd className="inline">
                  The professional social network:{" "}
                  <a
                    className="text-orange-600"
                    href="https://www.linkedin.com/in/david-beijinho-20997360"
                    target="_blank"
                    rel="noreferrer"
                  >
                    https://www.linkedin.com/in/david-beijinho-20997360
                  </a>
                </dd>
              </div>
              <div className="relative pl-9">
                <dt className="inline font-semibold text-gray-900">
                  <MailIcon
                    className="absolute left-1 top-1 h-5 w-5 text-orange-600"
                    aria-hidden="true"
                  />
                  The email.
                </dt>
                <dd className="inline">
                  For more direct contact:{" "}
                  <a
                    className="text-orange-600"
                    href="mailto:david@thebeijinho.com"
                    target="_blank"
                    rel="noreferrer"
                  >
                    david@thebeijinho.com
                  </a>
                </dd>
              </div>
            </dl>
          </div>
        </div>
        <Image
          src="/images/books.webp"
          alt="Contacts image"
          className="w-[40rem] rounded-xl shadow-xl ring-1 ring-gray-400/10"
          width={1000}
          height={1000}
        />
      </div>
    </div>
  );
}
