import CategoryContainer from "@/components/CategoryContainer";
import BlogCard from "@/components/BlogCard";
import DataBlogs from "@/data/blog.json";

export default function Page() {
  return (
    <CategoryContainer title="Blog" subTitle="A place where i drop some ideas.">
      <div className="mt-5 border-t border-gray-200 pt-10 sm:mt-8 sm:pt-8">
        {DataBlogs.map((blog) => (
          <BlogCard blog={blog.data} key={blog.data.slug} />
        ))}
      </div>
    </CategoryContainer>
  );
}
