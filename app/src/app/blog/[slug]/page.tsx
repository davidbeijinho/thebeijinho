import MDXRender from "@/components/MDXRender";
import ContentPage from "@/components/ContentPage";
import DataBlogs from "@/data/blog.json";
import { serialize } from "next-mdx-remote/serialize";
import { notFound } from "next/navigation";

export async function generateStaticParams() {
  return DataBlogs.map(({ data }) => ({
    slug: data.slug,
  }));
}

export default async function Page({ params }: { params: { slug: string } }) {
  const post = DataBlogs.find(({ data }) => data.slug === params.slug);
  if (!post) {
    return notFound();
  }
  const source = await serialize(post.content);
  return (
    <ContentPage
      title={post.data.title}
      imageSrc={post.data.imageSrc}
      tags={post.data.tags}
    >
      {post.data.date.substring(0, 10)} / {post.data.category}
      <hr />
      <MDXRender content={source} />
    </ContentPage>
  );
}
