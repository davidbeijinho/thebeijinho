import MDXRender from "@/components/MDXRender";
import ContentPage from "@/components/ContentPage";
import ProjectsData from "@/data/projects.json";
import { serialize } from "next-mdx-remote/serialize";
import { notFound } from "next/navigation";

export async function generateStaticParams() {
  return ProjectsData.map(({ data }) => ({
    slug: data.slug,
  }));
}

export default async function Page({ params }: { params: { slug: string } }) {
  const project = ProjectsData.find(({ data }) => data.slug === params.slug);
  if (!project) {
    notFound();
  }
  const source = await serialize(project.content);
  return (
    <ContentPage
      title={project.data.title}
      imageSrc={project.data.imageSrc}
      tags={project.data.tags}
    >
      <hr />
      <MDXRender content={source} />
    </ContentPage>
  );
}
