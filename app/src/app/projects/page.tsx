import CategoryContainer from "@/components/CategoryContainer";
import ProjectCard from "@/components/ProjectCard";
import ProjectsData from "@/data/projects.json";

export default function Page() {
  return (
    <CategoryContainer
      title="Projects"
      subTitle="An archive of ideas and plans."
    >
      <div className="mx-auto mt-10 grid max-w-2xl grid-cols-1 gap-x-8 gap-y-16 border-t border-gray-200 pt-10 sm:mt-8 sm:pt-8 lg:mx-0 lg:max-w-none lg:grid-cols-3">
        {ProjectsData.map((project) => (
          <ProjectCard project={project.data} key={project.data.slug} />
        ))}
      </div>
    </CategoryContainer>
  );
}
