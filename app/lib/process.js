/* eslint-disable @typescript-eslint/no-var-requires */
const matter = require("gray-matter");
const fs = require("fs").promises;
const path = require("path");

async function processContent(content) {
  const parsed = matter(content, { excerpt: false });
  if (!parsed.data.slug) {
    throw "no slug provided";
  }
  return parsed;
}

async function processFile(filePath) {
  const content = await fs.readFile(filePath, "utf-8");
  return await processContent(content);
}

async function readDirectory(dir, fn) {
  const results = [];
  const entries = await fs.readdir(dir, { withFileTypes: true });
  for (const entry of entries) {
    const fullPath = path.join(dir, entry.name);
    if (entry.isDirectory()) {
      const subDirResults = await readDirectory(fullPath, fn);
      results.push(...subDirResults);
    } else {
      const response = await processFile(fullPath);
      results.push(response);
    }
  }
  return results;
}

async function processDataDir({ dir, out }) {
  const data = await readDirectory(dir, processFile);
  try {
    await fs.writeFile(
      out,
      JSON.stringify(
        data.sort((a, b) => a.data.date - b.data.date).reverse(),
        null,
        2,
      ),
      "utf8",
    );
    console.log("JSON file has been saved.");
  } catch (err) {
    console.log("An error occurred while writing JSON Object to File.");
    console.error(err);
  }
}

async function main(data) {
  for (const entry of data) {
    await processDataDir(entry);
    console.log(entry);
  }
}

const structure = [
  {
    dir: "./content/blog",
    out: "./src/data/blog.json",
  },
  {
    dir: "./content/projects",
    out: "./src/data/projects.json",
  },
];

main(structure);
