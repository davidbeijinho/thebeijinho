#!/usr/bin/env bash

full_path=$(realpath $0)
dir_path=$(dirname $full_path)

$dir_path/pocketbase serve --http=0.0.0.0:3005
