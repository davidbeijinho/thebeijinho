#! /usr/bin/env bash

PB_VERSION=0.22.14
PB_ARCH=linux_amd64

wget https://github.com/pocketbase/pocketbase/releases/download/v${PB_VERSION}/pocketbase_${PB_VERSION}_${PB_ARCH}.zip -O pb.zip
unzip -o ./pb.zip -d .
